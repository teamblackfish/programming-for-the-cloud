﻿using System.Collections.Generic;
using Ozyra.DependencyInjection.Attributes;

namespace IsaacHili.Academic.Cloud.Services
{
	[SingletonService]
	public class FileTypeValidationService
	{
		private readonly IDictionary<string, byte?[][]> _signatures = new Dictionary<string, byte?[][]>
		{
			{
				"jpg", new []
				{
					new byte?[] { 0xFF, 0xD8, 0xFF, 0xDB },
					new byte?[] { 0xFF, 0xD8, 0xFF, 0xE0, null, null, 0x4A, 0x46, 0x49, 0x46, 0x00, 0x01 },
					new byte?[] { 0xFF, 0xD8, 0xFF, 0xE1, null, null, 0x45, 0x78, 0x69, 0x66, 0x00, 0x00 }
				}
			},
			{
				"png", new[]
				{
					new byte?[] { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A }
				}
			},
			{
				"gif", new []
				{
					new byte?[] { 0x47, 0x49, 0x46, 0x38, 0x37, 0x61 },
					new byte?[] { 0x47, 0x49, 0x46, 0x38, 0x39, 0x61 }
				}
			},
			{
				"bmp", new []
				{
					new byte?[] { 0x42, 0x4D }
				}
			},
			{
				"webp", new []
				{
					new byte?[] { 0x52, 0x49, 0x46, 0x46, null, null, null, null, 0x57, 0x45, 0x42, 0x50 }
				}
			}
		};

		private readonly IDictionary<string, string> _pairs = new Dictionary<string, string>
		{
			{
				"jpeg", "jpg"
			},
			{
				"dib", "bmp"
			}
		};

		public bool IsValid(byte[] content, string fileType)
		{
			if (string.IsNullOrWhiteSpace(fileType))
				return false;

			fileType = fileType.Trim();
			fileType = fileType[0] == '.' 
				? fileType.Substring(1)
				: fileType;

			if (!_signatures.ContainsKey(fileType))
			{
				if (!_pairs.ContainsKey(fileType))
					return false;

				fileType = _pairs[fileType];
			}

			bool isValid = false;

			foreach (byte?[] signature in _signatures[fileType])
			{
				if (isValid)
					break;

				if (content.Length < signature.Length)
					continue;

				for (int i = 0; i < signature.Length; i++)
					if (!(isValid = signature[i] == content[i] || signature[i] == null))
						break;
			}

			return isValid;
		}
	}
}
