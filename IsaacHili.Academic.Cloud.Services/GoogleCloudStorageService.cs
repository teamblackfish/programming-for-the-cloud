﻿using System;
using System.IO;
using System.Threading.Tasks;
using Google.Cloud.Storage.V1;
using IsaacHili.Academic.Cloud.Models.Configurations;
using Microsoft.Extensions.Options;
using Ozyra.DependencyInjection.Attributes;

namespace IsaacHili.Academic.Cloud.Services
{
	[ScopedService]
	public class GoogleCloudStorageService
	{
		private readonly IOptions<GoogleCloudStorageSettings> _options;
		private readonly StorageClient _storageClient;

		public GoogleCloudStorageService(IOptions<GoogleCloudStorageSettings> options)
		{
			_options = options;
			_storageClient = StorageClient.Create();
		}

		public async Task<string> Download(string resourceName)
		{
			using (MemoryStream stream = new MemoryStream())
			{
				await _storageClient.DownloadObjectAsync(_options.Value.BucketName, resourceName, stream);

				byte[] content = new byte[stream.Length];
				stream.Seek(0, SeekOrigin.Begin);
				stream.Read(content, 0, content.Length);

				return Convert.ToBase64String(content);
			}
		}

		public async Task<bool> Upload(Stream content, string resourceName)
		{
			try
			{
				await _storageClient.UploadObjectAsync(_options.Value.BucketName, resourceName, "text/plain", content);
				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}
