﻿using System;
using System.IO;
using System.Threading.Tasks;
using IsaacHili.Academic.Cloud.DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Ozyra.DependencyInjection.Attributes;
using Ozyra.Web.Responses;

namespace IsaacHili.Academic.Cloud.Services
{
	[ScopedService]
	public class DailyUploadService
	{
		private const string Key = "daily-upload";

		private readonly DailyUploadDataAccess _dataAccess;
		private readonly GoogleCloudStorageService _storageService;
		private readonly FileTypeValidationService _validationService;
		private readonly ResponseMessageCollection _messageCollection;
		private readonly IMemoryCache _memoryCache;

		public DailyUploadService(DailyUploadDataAccess dataAccess, GoogleCloudStorageService storageService, FileTypeValidationService validationService, ResponseMessageCollection messageCollection, IMemoryCache memoryCache)
		{
			_dataAccess = dataAccess;
			_storageService = storageService;
			_validationService = validationService;
			_messageCollection = messageCollection;
			_memoryCache = memoryCache;
		}

		public async Task<string> Download()
		{
			if (_memoryCache.TryGetValue(Key, out string image))
				return image;

			string name = _dataAccess.Get();

			if (name == null)
				return null;

			string base64 = await _storageService.Download(name);
			_memoryCache.Set(Key, base64);

			return base64;
		}

		public async Task<(string, bool)> Upload(IFormFile file)
		{
			string fileType = Path.GetExtension(file.FileName);
			string name = $"{Guid.NewGuid():N}{fileType}";

			using (MemoryStream stream = new MemoryStream())
			{
				file.CopyTo(stream);
				stream.Seek(0, SeekOrigin.Begin);

				byte[] content = new byte[stream.Length];
				stream.Read(content, 0, content.Length);

				if (!_validationService.IsValid(content, fileType))
				{
					_messageCollection.Add("unsupported-file-type");
					return (null, false);
				}

				await _storageService.Upload(stream, name);
				string base64 = await _storageService.Download(name);

				_dataAccess.Save(name);
				_memoryCache.Set(Key, base64);

				return (base64, true);
			}
		}
	}
}
