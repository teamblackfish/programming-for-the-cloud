﻿using System.IdentityModel.Tokens.Jwt;
using IsaacHili.Academic.Cloud.DataAccess;
using IsaacHili.Academic.Cloud.Models.Tables;
using Ozyra.DependencyInjection.Attributes;

namespace IsaacHili.Academic.Cloud.Services
{
	[ScopedService]
	public class UserService
	{
		private readonly UserDataAccess _userDataAccess;

		public UserService(UserDataAccess userDataAccess)
		{
			_userDataAccess = userDataAccess;
		}

		public User Authenticate(JwtPayload payload, out bool isSuccess) => _userDataAccess.Authenticate(new User
		{
			GooglePlusId = payload.Sub,
			Email = payload["email"].ToString(),
			FirstName = payload["given_name"].ToString(),
			LastName = payload["family_name"].ToString()
		}, out isSuccess);
	}
}
