﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Cloud.PubSub.V1;
using IsaacHili.Academic.Cloud.Models.Configurations;
using Microsoft.Extensions.Options;
using Ozyra.DependencyInjection.Attributes;

namespace IsaacHili.Academic.Cloud.Services
{
	[SingletonService]
	public class GoogleCloudPublisherService
	{
		private readonly PublisherServiceApiClient _client;
		private readonly IDictionary<string, PublisherClient> _publishers;
		private readonly IOptions<GoogleCloudPubSubSettings> _settings;

		public GoogleCloudPublisherService(IOptions<GoogleCloudPubSubSettings> settings)
		{
			_settings = settings;
			_client = PublisherServiceApiClient.Create();
			_publishers = new Dictionary<string, PublisherClient>();

			Subscribe(_settings.Value.Topics.ToArray());
		}

		public void Subscribe(params string[] topicNames)
		{
			foreach (string topicName in topicNames)
			{
				if (_publishers.ContainsKey(topicName))
					throw new ArgumentException($"Topic {topicName} already exists.", nameof(topicName));

				PublisherClient publisher = PublisherClient.Create(new TopicName(_settings.Value.Project, topicName), new[] { _client });
				_publishers.Add(topicName, publisher);
			}
		}

		public async Task Publish(string topicName, params string[] messages)
		{
			if (!_publishers.ContainsKey(topicName))
				Subscribe(topicName);

			PublisherClient publisher = _publishers[topicName];

			foreach (string message in messages)
				await publisher.PublishAsync(message);
		}
	}
}
