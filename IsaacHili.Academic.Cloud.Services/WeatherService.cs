﻿using IsaacHili.Academic.Cloud.DataAccess;
using IsaacHili.Academic.Cloud.Models.Tables;
using Newtonsoft.Json;
using Ozyra.DependencyInjection.Attributes;

namespace IsaacHili.Academic.Cloud.Services
{
	[ScopedService]
	public class WeatherService
	{
		private readonly WeatherDataAccess _weatherDataAccess;
		private readonly GoogleCloudPublisherService _publisherService;

		public WeatherService(WeatherDataAccess weatherDataAccess, GoogleCloudPublisherService publisherService)
		{
			_weatherDataAccess = weatherDataAccess;
			_publisherService = publisherService;
		}

		public WeatherReport Latest() => _weatherDataAccess.Latest();

		public bool Create(WeatherReport weatherReport)
		{

			bool isSuccess = _weatherDataAccess.Create(weatherReport);

			if (isSuccess)
				_publisherService.Publish("weather", JsonConvert.SerializeObject(weatherReport)).RunSynchronously();

			return isSuccess;
		}
	}
}
