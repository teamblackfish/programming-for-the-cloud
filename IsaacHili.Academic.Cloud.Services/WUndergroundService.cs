﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IsaacHili.Academic.Cloud.Models.Configurations;
using IsaacHili.Academic.Cloud.Models.Tables;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Ozyra.DependencyInjection.Attributes;

namespace IsaacHili.Academic.Cloud.Services
{
	[SingletonService]
    public class WUndergroundService
	{
		private const string BaseUri = "http://api.wunderground.com/api/";
		private const string ConditionsEndpoint = "conditions/q/";
		private const string Location = "MT/Luqa.json";

		private readonly HttpClient _client;
		private readonly IOptions<WUndergroundSettings> _options;

		private string BaseUriApi => $"{BaseUri}{_options.Value.Key}/";

		private string ConditionsUri => BaseUriApi + ConditionsEndpoint + Location;

		public WUndergroundService(IOptions<WUndergroundSettings> options)
		{
			_options = options;
			_client = new HttpClient();
		}

		public async Task<WeatherReport> Current()
		{
			JObject current = (JObject.Parse(await (await _client.GetAsync(ConditionsUri)).Content.ReadAsStringAsync()) as dynamic).current_observation;

			WeatherReport report = new WeatherReport
			{
				Location = current["display_location"].Value<string>("full"),
				ObservationTime = DateTimeOffset.FromUnixTimeMilliseconds(current.Value<long>("observation_epoch") * 1000),
				Condition = current.Value<string>("icon"),
				Temperature = current.Value<double>("temp_c"),
				WindDirection = current.Value<string>("wind_dir"),
				WindSpeed = current.Value<double>("wind_kph")
			};

			return report;
		}
	}
}
