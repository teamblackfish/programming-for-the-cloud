﻿using System.IdentityModel.Tokens.Jwt;
using Ozyra.DependencyInjection.Attributes;

namespace IsaacHili.Academic.Cloud.Services
{
	[ScopedService]
    public sealed class TokenService
	{
		private readonly JwtSecurityTokenHandler _tokenHandler;

	    public TokenService()
	    {
			_tokenHandler = new JwtSecurityTokenHandler();
	    }

		public JwtPayload Decode(string token) => _tokenHandler.ReadJwtToken(token).Payload;
	}
}
