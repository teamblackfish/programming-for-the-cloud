﻿using System.Threading.Tasks;
using IsaacHili.Academic.Cloud.Models.Tables;
using Ozyra.DependencyInjection.Attributes;

namespace IsaacHili.Academic.Cloud.Services.Tasks
{
	[ScopedService]
	public class WeatherTask
	{
		private readonly WUndergroundService _wUndergroundService;
		private readonly WeatherService _weatherService;

		public WeatherTask(WUndergroundService wUndergroundService, WeatherService weatherService)
		{
			_wUndergroundService = wUndergroundService;
			_weatherService = weatherService;
		}

		public async Task ExecuteAsync()
		{
			WeatherReport weatherReport = await _wUndergroundService.Current();
			_weatherService.Create(weatherReport);
		}
	}
}
