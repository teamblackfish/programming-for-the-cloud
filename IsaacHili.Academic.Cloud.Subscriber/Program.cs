﻿using System;
using System.Globalization;
using System.Text;
using Google.Cloud.PubSub.V1;
using IsaacHili.Academic.Cloud.Models.Tables;
using Newtonsoft.Json;

namespace IsaacHili.Academic.Cloud.Subscriber
{
	internal class Program
	{
		internal static void Main(string[] args)
		{
			SubscriptionName subscription = new SubscriptionName("progforcloud-swd63b-4", "weather");
			SubscriberServiceApiClient client = SubscriberServiceApiClient.Create();
			SubscriberClient subscriber = SubscriberClient.Create(subscription, new[] { client });

			subscriber.StartAsync(async (message, token) =>
			{
				WeatherReport weather = JsonConvert.DeserializeObject<WeatherReport>(Encoding.UTF8.GetString(message.Data.ToByteArray()));
				await Console.Out.WriteAsync(weather.Temperature.ToString(CultureInfo.CurrentCulture));
				return SubscriberClient.Reply.Ack;
			}).Wait();

			Console.ReadKey();
		}
	}
}
