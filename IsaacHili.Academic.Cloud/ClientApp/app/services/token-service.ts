import { autoinject } from "aurelia-framework";
import { RequestService } from "./request-service";

export class TokenService {
	private get authorization(): string {
		return 'authorization';
	}

	public get(): string | null {
		return localStorage.getItem(this.authorization);
	}

	public set(token: string) {
		localStorage.setItem(this.authorization, token);
	}

	public remove() {
		localStorage.removeItem(this.authorization);
	}
}