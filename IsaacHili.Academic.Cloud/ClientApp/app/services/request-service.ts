import { HttpClient, RequestInit } from "aurelia-fetch-client";
import { autoinject } from "aurelia-framework";
import { TokenService } from "./token-service";

@autoinject
export class RequestService extends HttpClient {
	constructor(private tokenService: TokenService) {
		super();
		this.configure(c => c
			.withBaseUrl('api/')
			.withDefaults({
				headers: {
					Accept: 'application/json'
				}
			}));
	}

	public fetch(input: Request | string, init?: RequestInit): Promise<Response> {
		(<any>this.defaults.headers)['Authorization'] = this.tokenService.get();
		return super.fetch(input, init);
	}
}