export class RoleService {
	private get role(): string {
		return 'role';
	}

	public get(): string | null {
		return localStorage.getItem(this.role);
	}

	public set(token: string) {
		localStorage.setItem(this.role, token);
	}

	public remove() {
		localStorage.removeItem(this.role);
	}
}