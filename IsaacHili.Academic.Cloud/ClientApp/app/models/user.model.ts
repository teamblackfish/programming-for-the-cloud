import { Guid } from "guid-typescript";

export interface User {
	id: Guid;
	email: string;
	googlePlusId: string;
	firstName: string;
	lastName: string;
	isAdmin: boolean;
}