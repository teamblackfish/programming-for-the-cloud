export interface JsonResponse<TData> {
	meta: {
		timestamp: number;
		isError: boolean;
		pageNumber: number;
		pageLimit: number;
		messages: string[];
	};
	data: TData;
}