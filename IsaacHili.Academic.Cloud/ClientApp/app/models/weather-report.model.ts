import { Guid } from 'guid-typescript';

export interface WeatherReport {
	id: Guid;
	location: string;
	observationTime: Date;
	condition: string;
	temperature: number;
	windDirection: string;
	windSpeed: number;
}