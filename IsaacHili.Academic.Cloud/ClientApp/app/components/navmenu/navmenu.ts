import { Router } from "aurelia-router";
import { TokenService } from "../../services/token-service";
import { autoinject, computedFrom } from "aurelia-framework";
import { RoleService } from "../../services/role-service";

@autoinject
export class Navmenu {
	public get hasToken(): boolean {
		return !!this.tokenService.get();
	}

	public constructor(public router: Router, private tokenService: TokenService, private roleService: RoleService) { }

	public logout() {
		this.tokenService.remove();
		this.roleService.remove();
	}
}