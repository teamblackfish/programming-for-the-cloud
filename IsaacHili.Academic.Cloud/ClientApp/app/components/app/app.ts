import { Aurelia, PLATFORM, autoinject } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';

@autoinject
export class App {
    public constructor(private router: Router){

    }

    configureRouter(config: RouterConfiguration, router: Router) {
        config.title = 'Weather';
        config.map([{
            route: [ '', 'home', 'weather' ],
            name: 'weather',
            settings: { icon: 'weather' },
            moduleId: PLATFORM.moduleName('../weather/weather'),
            nav: true,
            title: 'Home'
        }]);

        this.router = router;
    }
}
