import { autoinject, computedFrom } from "aurelia-framework";
import { WeatherReport } from "../../models/weather-report.model";
import { JsonResponse } from "../../models/json-response.model";
import { TokenService } from "../../services/token-service";
import { RoleService } from "../../services/role-service";
import { RequestService } from "../../services/request-service";

@autoinject
export class Home {
	public weatherReport?: WeatherReport | null;
	public isVariable: boolean = false;
	public file?: HTMLInputElement;
	public image: string | null = null;

	@computedFrom('weatherReport', 'weatherReport.windDirection')
	public get compassPointerRotation(): number {
		const offset = -90;

		if (!this.weatherReport)
			return offset;
		
		let value = 0;
		const angle = 22.5;

		switch(this.weatherReport.windDirection) {
			case 'NNW':
				value += angle;
			case 'NW':
				value += angle;
			case 'WNW':
				value += angle;
			case 'W':
			case 'West':
				value += angle;
			case 'WSW':
				value += angle;
			case 'SW':
				value += angle;
			case 'SSW':
				value += angle;
			case 'S':
			case 'South':
				value += angle;
			case 'SSE':
				value += angle;
			case 'SE':
				value += angle;
			case 'ESE':
				value += angle;
			case 'E':
			case 'East':
				value += angle;
			case 'ENE':
				value += angle;
			case 'NE':
				value += angle;
			case 'NNE':
				value += angle;
				break;
			case 'Variable':
				this.isVariable = true;
				break;
		}

		return value + offset - 180;
	}

	@computedFrom('weatherReport', 'weatherReport.condition')
	public get icon(): string {
		if (!this.weatherReport)
			return 'storm';

		switch(this.weatherReport.condition) {
			case 'sunny':
			case 'mostlysunny':
			case 'clear':
			case 'hazy':
				return 'clear';
			case 'partlycloudy':
				return 'partly_cloudy';
			case 'cloudy':
			case 'fog':
				return 'cloudy';
			case 'rain':
			case 'chancerain':
				return 'rain';
			case 'snow':
			case 'sleet':
			case 'flurries':
			case 'chancesnow':
			case 'chancesleet':
			case 'chanceflurries':
				return 'snow';
			case 'tstorms':
			case 'chancetstorms':
				return 'thunderstorm';
		}

		return 'storm';
	}

	public get isAdmin(): boolean {
		return !!this.tokenService.get() && this.roleService.get() == 'Administrator';
	}

	public constructor(private requestService: RequestService, private tokenService: TokenService, private roleService: RoleService) { }

	async activate() {
		const response = await this.requestService.fetch('weather');
		this.weatherReport = (await response.json() as JsonResponse<WeatherReport>).data;

		const imageResponse = await this.requestService.fetch("dailyUpload");
		this.image = ((await imageResponse.json()) as JsonResponse<string>).data;
	}

	async updateImage(e: Event) {
		const upload = async (file: File): Promise<boolean> => {
			const form = new FormData();
			form.append('file', file);

			try {
				const response = await this.requestService.fetch('dailyUpload', {
					method: 'post',
					body: form
				});

				this.image = ((await response.json()) as JsonResponse<string>).data;
				
				return true;
			} catch {
				return false;
			}
		};

		return new Promise<void>((resolve, reject) => {
			if (!this.file) {
				reject();
				return;
			}

			this.file.click();

			this.file.onchange = async () => {
				if (!(this.file && this.file.files && this.file.files.length > 0)) {
					reject();
					return;
				}

				const response = await upload(this.file.files[0]);
				this.file.value = '';

				response 
					? resolve()
					: reject();
			};
		});
	}
}
