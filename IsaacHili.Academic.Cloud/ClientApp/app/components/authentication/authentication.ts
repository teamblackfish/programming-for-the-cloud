import { TokenService } from "../../services/token-service";
import { autoinject } from "aurelia-framework";
import { RequestService } from "../../services/request-service";
import { RoleService } from "../../services/role-service";
import { JsonResponse } from "../../models/json-response.model";
import { User } from "../../models/user.model";

@autoinject
export class Authentication {
	public signIn?: Element;

	public constructor(private tokenService: TokenService, private roleService: RoleService, private requestService: RequestService) { }

	public attached() {
		let auth2: gapi.auth2.GoogleAuth;

		const attach = () => {
			auth2.attachClickHandler(this.signIn, {}, async (googleUser: gapi.auth2.GoogleUser) => {
				this.tokenService.set(googleUser.getAuthResponse().id_token);
				const response = await (await this.requestService.fetch('user', {
					method: 'post'
				})).json() as JsonResponse<User>;
				this.roleService.set(response.data.isAdmin ? 'Administrator' : 'User');
			}, (error: string) => {
				console.log(error);
			})
		}

		gapi.load('auth2', () => {
			auth2 = gapi.auth2.init({
				client_id: '691604560702-5n28pmv726raco471huvr7ktbg0qpe0d.apps.googleusercontent.com',
				cookie_policy: 'single_host_origin'
			});
			attach();
		});
	}
}
