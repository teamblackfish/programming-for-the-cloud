using Hangfire;
using IsaacHili.Academic.Cloud.Models;
using IsaacHili.Academic.Cloud.Models.Configurations;
using IsaacHili.Academic.Cloud.Services.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ozyra.DependencyInjection;

namespace IsaacHili.Academic.Cloud
{
	public class Startup
	{
		private const string GoogleClientId = "Authentication:Google:ClientId";
		private const string GoogleClientSecret = "Authentication:Google:ClientSecret";
		private const string ConnectionString = "Database:ConnectionString";

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc();
			services.AddAttributeDependencyInjection();
			services.AddHangfire(c => c.UseSqlServerStorage(Configuration[ConnectionString]));
			services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddGoogle(o =>
			{
				o.ClientId = Configuration[GoogleClientId];
				o.ClientSecret = Configuration[GoogleClientSecret];
			});
			services.AddMemoryCache();

			services.AddDbContext<DatabaseContext>(o => o.UseSqlServer(Configuration[ConnectionString], ob => ob.MigrationsAssembly("IsaacHili.Academic.Cloud")));
			services.AddScoped<DbContext, DatabaseContext>();

			services.AddSingleton<HttpContextAccessor>();
			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

			services.Configure<IISOptions>(o =>
			{
				o.AutomaticAuthentication = false;
				o.ForwardClientCertificate = true;
			});
			services.Configure<WUndergroundSettings>(o => Configuration.GetSection("WUnderground").Bind(o));
			services.Configure<GoogleCloudStorageSettings>(o => Configuration.GetSection("GoogleCloud:CloudStorage").Bind(o));
			services.Configure<GoogleCloudPubSubSettings>(o => Configuration.GetSection("GoogleCloud:PubSub").Bind(o));
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
				{
					HotModuleReplacement = true
				});
			}
			else
			{
				app.UseDeveloperExceptionPage();
				app.UseExceptionHandler("/Home/Error");
			}

			using (IServiceScope serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
			{
				var context = serviceScope.ServiceProvider.GetRequiredService<DbContext>();
				context.Database.Migrate();
				context.Database.EnsureCreated();
			}

			app.UseStaticFiles();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");

				routes.MapSpaFallbackRoute(
					name: "spa-fallback",
					defaults: new { controller = "Home", action = "Index" });
			});

			app.UseHangfireServer();

			RecurringJob.AddOrUpdate<WeatherTask>(nameof(WeatherTask), wt => wt.ExecuteAsync(), Cron.MinuteInterval(30));
		}
	}
}
