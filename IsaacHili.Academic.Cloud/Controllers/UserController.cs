﻿using IsaacHili.Academic.Cloud.Models.Tables;
using IsaacHili.Academic.Cloud.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ozyra.Web.Controllers;
using Ozyra.Web.Responses;

namespace IsaacHili.Academic.Cloud.Controllers
{
	[Route("api/[controller]")]
	public class UserController : BaseJsonController
	{
		private const string Authorization = "Authorization";

		private readonly TokenService _tokenService;
		private readonly UserService _userService;

		public UserController(IHttpContextAccessor httpContextAccessor, ResponseMessageCollection responseMessages, TokenService tokenService, UserService userService) : base(httpContextAccessor, responseMessages)
		{
			_tokenService = tokenService;
			_userService = userService;
		}

		[HttpPost]
		public User Authenticate() => 
			_userService.Authenticate(_tokenService.Decode(HttpContext.Request.Headers[Authorization]), out bool isSuccess);
	}
}
