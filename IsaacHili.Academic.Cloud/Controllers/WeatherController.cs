﻿using IsaacHili.Academic.Cloud.Models.Tables;
using IsaacHili.Academic.Cloud.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ozyra.Web.Controllers;
using Ozyra.Web.Responses;

namespace IsaacHili.Academic.Cloud.Controllers
{
	[Route("api/[controller]")]
	public class WeatherController : BaseJsonController
	{
		private readonly WeatherService _weatherService;

		public WeatherController(IHttpContextAccessor httpContextAccessor, ResponseMessageCollection responseMessages, WeatherService weatherService) : base(httpContextAccessor, responseMessages)
		{
			_weatherService = weatherService;
		}

		[HttpGet]
		public WeatherReport Current() => _weatherService.Latest();
	}
}
