﻿using System.Threading.Tasks;
using IsaacHili.Academic.Cloud.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ozyra.Web.Controllers;
using Ozyra.Web.Responses;

namespace IsaacHili.Academic.Cloud.Controllers
{
	[Route("api/[controller]")]
	public class DailyUploadController : BaseJsonController
	{
		private readonly DailyUploadService _service;

		public DailyUploadController(IHttpContextAccessor httpContextAccessor, ResponseMessageCollection responseMessages, DailyUploadService service) : base(httpContextAccessor, responseMessages)
		{
			_service = service;
		}

		[HttpGet]
		public async Task<string> Download() =>
			await _service.Download();

		[HttpPost]
		public async Task<string> Upload([FromForm] IFormFile file)
		{
			(string, bool) result = await _service.Upload(file);

			if (!result.Item2)
				HttpContextAccessor.HttpContext.Response.StatusCode = StatusCodes.Status415UnsupportedMediaType;

			return result.Item1;
		}
	}
}
