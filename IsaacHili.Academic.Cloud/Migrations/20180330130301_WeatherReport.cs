﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IsaacHili.Academic.Cloud.Migrations
{
    public partial class WeatherReport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "weather_reports",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Condition = table.Column<string>(nullable: false),
                    Location = table.Column<string>(nullable: false),
                    ObservationTime = table.Column<DateTimeOffset>(nullable: false),
                    Temperature = table.Column<double>(nullable: false),
                    WindDirection = table.Column<string>(nullable: false),
                    WindSpeed = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_weather_reports", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_weather_reports_ObservationTime",
                table: "weather_reports",
                column: "ObservationTime",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "weather_reports");
        }
    }
}
