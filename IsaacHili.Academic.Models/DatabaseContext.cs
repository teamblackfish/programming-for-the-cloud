﻿using IsaacHili.Academic.Cloud.Models.Tables;
using Microsoft.EntityFrameworkCore;

namespace IsaacHili.Academic.Cloud.Models
{
	public class DatabaseContext : DbContext
	{
		public DatabaseContext(DbContextOptions options) : base(options)
		{

		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.Entity<WeatherReport>()
				.ToTable("weather_reports")
				.HasIndex(wr => wr.ObservationTime)
				.IsUnique();

			builder.Entity<DailyUpload>()
				.ToTable("daily_uploads")
				.HasIndex(du => du.Date)
				.IsUnique();

			var users = builder.Entity<User>()
				.ToTable("users");

			users.HasIndex(u => u.GooglePlusId)
				.IsUnique();

			users.HasIndex(u => u.Email)
				.IsUnique();

			base.OnModelCreating(builder);
		}
	}
}
