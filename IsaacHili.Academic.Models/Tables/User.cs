﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IsaacHili.Academic.Cloud.Models.Tables
{
    public class User
    {
		[Key]
		public Guid Id { get; set; } = Guid.NewGuid();

	    public string Email { get; set; }

		public string FirstName { get; set; }

	    public string LastName { get; set; }

	    public string GooglePlusId { get; set; }

	    public bool IsAdmin { get; set; } = false;
    }
}
