﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IsaacHili.Academic.Cloud.Models.Tables
{
	public class WeatherReport
	{
		[Key]
		public Guid Id { get; set; } = Guid.NewGuid();

		[Required]
		public string Location { get; set; }

		[Required]
		public DateTimeOffset ObservationTime { get; set; }

		[Required]
		public string Condition { get; set; }

		[Required]
		public double Temperature { get; set; }

		[Required]
		public string WindDirection { get; set; }

		[Required]
		public double WindSpeed { get; set; }
	}
}
