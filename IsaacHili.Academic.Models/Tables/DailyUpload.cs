﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IsaacHili.Academic.Cloud.Models.Tables
{
	public class DailyUpload
	{
		[Key]
		public Guid Id { get; set; } = Guid.NewGuid();

		[Required]
		public DateTime Date { get; set; } = DateTime.Now;

		[Required]
		public string ResourceName { get; set; }
	}
}
