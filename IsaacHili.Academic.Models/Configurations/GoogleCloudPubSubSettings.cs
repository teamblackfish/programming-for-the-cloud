﻿using System.Collections.Generic;

namespace IsaacHili.Academic.Cloud.Models.Configurations
{
	public class GoogleCloudPubSubSettings
	{
		public string Project { get; set; }

		public IEnumerable<string> Topics { get; set; }
	}
}
