﻿using System;
using System.Linq;
using IsaacHili.Academic.Cloud.Models;
using IsaacHili.Academic.Cloud.Models.Tables;
using Ozyra.DependencyInjection.Attributes;

namespace IsaacHili.Academic.Cloud.DataAccess
{
	[ScopedService]
	public class DailyUploadDataAccess
	{
		private readonly DatabaseContext _databaseContext;

		public DailyUploadDataAccess(DatabaseContext databaseContext)
		{
			_databaseContext = databaseContext;
		}

		public string Get() =>
			_databaseContext.Set<DailyUpload>()
				.Where(du => du.Date.Date == DateTime.Today.Date)
				.OrderByDescending(du => du.Date)
				.FirstOrDefault()?
				.ResourceName;

		public void Save(string resourceName)
		{
			_databaseContext.Set<DailyUpload>()
				.Add(new DailyUpload
				{
					ResourceName = resourceName
				});

			_databaseContext.SaveChanges();
		}
	}
}
