﻿using System.Linq;
using IsaacHili.Academic.Cloud.Models.Tables;
using Microsoft.EntityFrameworkCore;
using Ozyra.DependencyInjection.Attributes;

namespace IsaacHili.Academic.Cloud.DataAccess
{
	[ScopedService]
	public class WeatherDataAccess
	{
		private readonly DbContext _dbContext;

		public WeatherDataAccess(DbContext dbContext)
		{
			_dbContext = dbContext;
		}

		public WeatherReport Latest() =>
			_dbContext.Set<WeatherReport>()
				.OrderByDescending(wr => wr.ObservationTime)
				.FirstOrDefault();

		public bool Create(WeatherReport weatherReport)
		{
			var reports = _dbContext.Set<WeatherReport>();

			if (reports.Any(wr => wr.ObservationTime.Ticks == weatherReport.ObservationTime.Ticks))
				return false;

			reports.Add(weatherReport);

			try
			{
				_dbContext.SaveChanges();
			}
			catch
			{
				return false;
			}

			return true;
		}
	}
}
