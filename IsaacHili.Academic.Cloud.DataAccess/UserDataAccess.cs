﻿using System.Linq;
using IsaacHili.Academic.Cloud.Models;
using IsaacHili.Academic.Cloud.Models.Tables;
using Ozyra.DependencyInjection.Attributes;

namespace IsaacHili.Academic.Cloud.DataAccess
{
	[ScopedService]
	public class UserDataAccess
	{
		private readonly DatabaseContext _databaseContext;

		public UserDataAccess(DatabaseContext databaseContext)
		{
			_databaseContext = databaseContext;
		}

		public User Authenticate(User user, out bool isSuccess)
		{
			if (user == null)
			{
				isSuccess = false;
				return null;
			}

			var users = _databaseContext.Set<User>();
			User crudUser = users.FirstOrDefault(u => u.GooglePlusId == user.GooglePlusId);

			if (crudUser != null)
			{
				crudUser.FirstName = user.FirstName;
				crudUser.LastName = user.LastName;
				users.Update(crudUser);
			}
			else
			{
				crudUser = user;
				users.Add(crudUser);
			}

			try
			{
				_databaseContext.SaveChanges();
			}
			catch
			{
				isSuccess = false;
				return null;
			}

			isSuccess = true;
			return crudUser;
		}
	}
}
